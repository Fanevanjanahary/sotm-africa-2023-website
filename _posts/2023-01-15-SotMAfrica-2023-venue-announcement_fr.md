---
layout: post
title: State of the Map Africa 2023 will be held in Yaoundé Cameroon
date: 2023-01-15 08:00:00
image: yaounde_2.jpg
isStaticPost: false
---

![](../img/posts/sotm2023_venue_announcement.png)

En Décembre 2023, la communauté OSM Afrique se réunira pour une autre conférence passionnante : State of the Map Africa 2023. Cette conférence a pour but de rassembler les passionnés d'OpenStreetMap d'Afrique et d'ailleurs. La conférence sera l'occasion pour les cartographes de partager et d'échanger des idées, des connaissances et des expériences, d'élargir leur réseau et de dégager des idées pour développer OSM sur le continent. Nous invitons les membres de la communauté OpenStreetMap à partager avec nous leurs projets et idées en matière de cartographie, ainsi que leurs expériences OSM, tant au niveau individuel que communautaire. Qu'il s'agisse de cartographes débutants, d'utilisateurs de données OSM ou de leaders communautaires, nous aimerions avoir de vos nouvelles!
