---
layout: post
title: State of the Map Africa 2023 will be held in Yaoundé Cameroon
date: 2023-01-15 08:00:00
image: yaounde_2.jpg
isStaticPost: false
---

![](../img/posts/sotm2023_venue_announcement.png)

From 1st to 3rd December 2023, the OSM Africa community will be coming together for yet another exciting conference: State of the Map Africa, 2023. The conference aims to bring together OpenStreetMap enthusiasts from Africa and beyond. The conference will provide an opportunity for mappers to share and exchange ideas, knowledge, and experience; expand their network, and generate ideas to grow OSM within the continent. We are inviting members of the OpenStreetMap community and other OSM data users to share with us about your mapping projects and ideas, OSM experiences at an individual, community or institutional level. From beginner mappers, OSM data users, to community leaders, we would like to hear from you!
In addition to proposals on OSM contribution, usage, and development, with this year’s theme Open mapping as a support tool for local development in Africa, we are keen to learn about mapping collaborations across different entities, communities, and countries.
